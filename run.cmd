@echo off
setlocal ENABLEEXTENSIONS
set "bot_name=GLaDOS DISCORD BOT"
set config="config.json"
set cmd_color=0c
title %bot_name%
color %cmd_color%
echo Launching %bot_name%
node "app.js"
timeout /t 30
exit