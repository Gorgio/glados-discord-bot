// App required modules
const Discord   = require('discord.js');
const fs        = require('fs');
const sqlite    = require('sqlite');

// Glados Bot Configuration
const config    = require('./config.json');
const PREFIX = config.prefix;
const DB_DIR = 'internal/';
const DB_FILE = 'glados.db';

// Internationalization
const i18n      = require('./locales/fr-fr.js');

// Glados Modules
const dice      = require('./glados-modules/dice/functions.js');
const rpg       = require('./glados-modules/rpg/functions.js');
const rpgConst  = require('./glados-modules/rpg/consts');
const askReplies= require('./glados-modules/ask/replies.json');
const emoji     = require('./glados-modules/emoji/emoji.json');
const sticker   = require('./glados-modules/sticker/sticker.json');

const CLIENT_OPTIONS = {
    disabledEvents:[
    'TYPING_START',
    'GUILD_ROLE_CREATE',
    'GUILD_ROLE_DELETE',
    'GUILD_ROLE_UPDATE'],
    messageCacheLifetime:(60*30)
};

const DiscordBotClient = new Discord.Client({CLIENT_OPTIONS});

/**
 * On message check if it starts with the prefix to execute commands:
 * See README for more information.
 */
DiscordBotClient.on('message', async message =>
{
    // Exit if the message doesn't start with the prefix or is send by a Bot.
    if ((message.content.indexOf(config.prefix) !== 0) || (message.author.bot === true)) {return;}
    // Splits the message into arguments and retrieves the command name without the prefix.
    let member = message.member;
    let arguments = message.content.slice(PREFIX.length).split(/ +/);
    let commandName = arguments[0].toLowerCase();
    let guild = message.guild;
    let channel = message.channel;

    const reply = async () => {
        /** Admin commands */
        if (member.permissions.has('ADMINISTRATOR')) {
            if (commandName === 'kill') {
                DiscordBotClient.destroy().catch(error => console.error(error));
                return null;
            }

            if (commandName === 'post') {
                // Parse message;
                let response = message.content.slice(commandName.length + 1);
                /** MessageMentions (message.mentions) object */
                if (message.mentions.channels.size !== 0) {
                    channel = message.mentions.channels.first();
                    response = response.slice(arguments[1].length + 1);
                } else {
                    channel = guild.defaultChannel;
                }
                return {message:response,channel:channel,noDelete:true};
            }
        }

        switch (commandName) {
            case 'age':
                if (message.mentions.members.size !== 0) {
                    member = message.mentions.members.first();
                }
                let guildJoinedAt = guild.member(member).joinedAt;
                return {message:{embed:i18n.AGE_USER(member, guildJoinedAt)}};

            case 'ageserv':
                return {message:i18n.getRelativeTimeFromNow(guild.createdAt, i18n.text.AGE_SERVER)};

            case 'ask':
                let replies = askReplies.yes.concat(
                    askReplies.doubtful,
                    askReplies.no,
                    askReplies.undecided,
                    askReplies.troll
                );
                let answer = replies[getRandomInt(0, replies.length - 1)];
                return { message:`[${member}] ${answer}`, noDelete:true};

            case 'e':
                if (arguments[1] === 'list') {
                    let list = Object.keys(emoji);
                    return { message:i18n.EMOJI_LIST + list.toString()};
                } else if (emoji[arguments[1].toLowerCase()] === undefined) {
                    return { message:i18n.EMOJI_UNKNOWN};
                } else {
                    return { message: {
                        embed: new Discord.RichEmbed()
                            .setTitle(emoji[arguments[1].toLowerCase()])
                            .setFooter(member.displayName, member.user.displayAvatarURL)
                    }}
                }

            case 's':
                if (arguments[1] === 'list') {
                    let list = Object.keys(sticker);
                    return { message:i18n.STICKER_LIST + list.toString()};
                } else if (sticker[arguments[1].toLowerCase()] === undefined) {
                    return { message:i18n.STICKER_UNKNOWN};
                } else {
                    return { message: {
                        embed: new Discord.RichEmbed()
                            .setImage(sticker[arguments[1].toLowerCase()])
                            .setFooter(member.displayName, member.user.displayAvatarURL)
                    }}
                }

            case 'help':
                return {message:{embed:i18n.HELP_EMBED(PREFIX)}};
				
			case 'helpgame':
                return {message:{embed:i18n.HELP_RPG_EMBED(PREFIX)}};

            case 'ping':
                return {message:{embed:i18n.PING_EMBED(member, DiscordBotClient.ping)}};

            /** Dice commands */
            case 'dstats':
                if (message.mentions.members.size !== 0) {
                    member = message.mentions.members.first();
                }
                let dbRequest = async ()  => {
                    try {
                        let dataDb = await dice.getStats(member.id);
                        return {message:{embed:i18n.DICE_STATS_EMBED(member, dataDb)}};
                    } catch (errorCode) {
                        return {message:i18n.text.DB_ERROR};
                    }
                };
                return await dbRequest();

            case 'd6':
            case 'd20':
            case 'd100':
                let rollType = Number(commandName.slice(1));
                let roll = getRandomInt(1, rollType);
                dice.insertRoll(member.id, rollType, roll);
                return {message:{embed:i18n.DICE_ROLL_EMBED(member, rollType, roll)}};

            /** RPG Commands */
            case 'daily':
                try {
                    let dailyData = await rpg.daily(member.id);
                    return {message:i18n.text.RPG_DAILY_RECEIVED.replace('{member}', member)
                        .replace('{amount}', dailyData.amount)}
                } catch (errorCode) {
                    switch (errorCode) {
                        case rpgConst.CODE_DAILY_TO_RECENT:
                            return {message:i18n.text.RPG_DAILY_TO_EARLY.replace('{member}', member)};

                        case rpgConst.CODE_DB_ERROR:
                            return {message:i18n.text.DB_ERROR};

                        default:
                            return {message:i18n.text.DEFAULT_ERROR};
                    }
                }
            case 'profile':
                if (message.mentions.members.size !== 0) {
                    member = message.mentions.members.first();
                }
                let getProfileResponse = async () => {
                    try {
                        let profileData = await rpg.getProfile(member.id);
                        if (profileData === null) {
                            return {message:i18n.text.RPG_MEMBER_NOT_REGISTERED.replace('{member}',member)};
                        } else {
                            return {message:{embed:i18n.RPG_PROFILE_EMBED(member, profileData)}};
                        }
                    } catch (errorCode) {
                        if (errorCode === rpgConst.CODE_TABLE_NOT_EXITING) {
                           return await getProfileResponse();
                        } else {
                            return {message:i18n.text.DB_ERROR};
                        }
                    }
                };
                return await getProfileResponse();

            case 'list':
                let page = Number(arguments[1]) > 0 ? Number(arguments[1]) : 1;
                let playersPerPage = rpgConst.PLAYER_PER_LIST_PAGE;
                let nbPlayers = await rpg.getNbPlayers();
                let totalPages = Math.ceil(nbPlayers/playersPerPage);
                let getListResponse = async () => {
                    try {
                        let listData = await rpg.getList(page, playersPerPage);
                        return {message:{embed:i18n.RPG_LIST_EMBED(listData, page, totalPages)}};
                    } catch (errorCode) {
                        return {message:i18n.text.DB_ERROR};
                    }
                };
                return getListResponse();

            case 'register':
                let getRegisterResponse = async () => {
                    try {
                        let isRegistered = await rpg.register(member.id);
                        let reply;
                        if (isRegistered === true) {
                            reply = i18n.text.RPG_REGISTER_SUCCESS;
                        } else {
                            reply = i18n.text.RPG_ALREADY_REGISTERED;
                        }
                        return {message:reply.replace('{member}', member)}
                    } catch (errorCode) {
                        if (errorCode === rpgConst.CODE_TABLE_NOT_EXITING) {
                            return await getRegisterResponse();
                        } else {
                            return {message:i18n.text.DB_ERROR};
                        }
                    }
                };
                return await getRegisterResponse();

            case 'attack':
                if (message.mentions.members.size === 0) {
                    return {message:i18n.text.RPG_SPECIFY_USER_TO_ATTACK};
                }
                let defender = message.mentions.members.first();
                try {
                    let dataAttack = await rpg.attack(member.id, defender.id);
                    let attackReply;
                    if (dataAttack.health <= 0) {
                        attackReply = i18n.RPG_ATTACK_KILL(defender, dataAttack);
                    } else {
                        attackReply = i18n.RPG_ATTACK(member, defender, dataAttack);
                    }
                    return {message:{embed:attackReply}};
                } catch (errorCode) {
                    switch (errorCode) {
                        case rpgConst.CODE_MEMBER_NOT_REGISTERED:
                            return {message:i18n.text.RPG_MEMBER_NOT_REGISTERED
                                .replace('{member}', defender)};

                        case rpgConst.CODE_SELF_NOT_REGISTERED:
                            return {message:i18n.text.RPG_SELF_NOT_REGISTERED};

                        case rpgConst.CODE_USER_IS_DEAD:
                            return {message:i18n.text.RPG_USER_IS_DEAD};

                        case rpgConst.CODE_NOT_ENOUGH_ENERGY:
                            return {message:i18n.text.RPG_NOT_ENOUGH_ENERGY};

                        case rpgConst.CODE_SELF_ATTACK:
                            return {message:i18n.text.RPG_SELF_ATTACK};

                        case rpgConst.CODE_DEFENDER_ALREADY_DEAD:
                            return {message:i18n.text.RPG_MEMBER_ALREADY_DEAD.replace('{member}', defender)};

                        case rpgConst.CODE_DB_ERROR:
                            return {message:i18n.text.DB_ERROR};

                        default:
                            return {message:i18n.text.DEFAULT_ERROR};
                    }
                }

            case 'heal':
                let receiver = message.mentions.members.size !== 0 ? message.mentions.members.first() : member;
                try {
                    let healData = await rpg.heal(member.id, receiver.id);
                    let healEmbed;
                    if (healData.self === true) {
                        healEmbed = i18n.RPG_HEAL_SELF(member, healData);
                    } else {
                        healEmbed = i18n.RPG_HEAL_MEMBER(member, receiver, healData);
                    }
                    return {message:{embed:healEmbed}};
                } catch (errorCode) {
                    switch (errorCode) {
                        case rpgConst.CODE_MEMBER_NOT_REGISTERED:
                            return {message:i18n.text.RPG_MEMBER_NOT_REGISTERED
                                .replace('{member}', receiver)};

                        case rpgConst.CODE_SELF_NOT_REGISTERED:
                            return {message:i18n.text.RPG_SELF_NOT_REGISTERED};

                        case rpgConst.CODE_USER_IS_DEAD:
                            return {message:i18n.text.RPG_USER_IS_DEAD};

                        case rpgConst.CODE_NOT_ENOUGH_ENERGY:
                            return {message:i18n.text.RPG_NOT_ENOUGH_ENERGY};

                        case rpgConst.CODE_CASTER_ALREADY_MAX_HEALTH:
                            return {message:i18n.text.RPG_SELF_MAX_HEALTH};

                        case rpgConst.CODE_RECEIVER_ALREADY_MAX_HEALTH:
                            return {message:i18n.text.RPG_MEMBER_MAX_HEALTH.replace('{member}', receiver)};

                        case rpgConst.CODE_RECEIVER_DEAD:
                            return {message:i18n.text.RPG_MEMBER_DEAD.replace('{member}', receiver)};

                        case rpgConst.CODE_DB_ERROR:
                            return {message:i18n.text.DB_ERROR};

                        default:
                            return {message:i18n.text.DEFAULT_ERROR};
                    }
                }

            case 'give':
                if (message.mentions.members.size === 0) {
                    return {message:i18n.text.RPG_SPECIFY_USER_TO_GIVE};
                } else if(Number(arguments[2]) <= 0) {
                    return {message:i18n.text.RPG_AMOUNT_TO_GIVE};
                } else {
                    let receiver = message.mentions.members.first();
                    let amount = Number(arguments[2]);
                    try {
                        let giveData = await rpg.give(member.id, receiver.id, amount);
                        if (giveData === true) {
                            return {message:{embed:i18n.RPG_MONEY(member, receiver, amount)}};
                        } else {
                            return {message:i18n.text.DEFAULT_ERROR};
                        }
                    } catch (errorCode) {
                        switch (errorCode) {
                            case rpgConst.CODE_MEMBER_NOT_REGISTERED:
                                return {message:i18n.text.RPG_MEMBER_NOT_REGISTERED
                                    .replace('{member}', receiver)};

                            case rpgConst.CODE_SELF_NOT_REGISTERED:
                                return {message:i18n.text.RPG_SELF_NOT_REGISTERED};

                            case rpgConst.CODE_SELF_GIVE:
                                return {message:i18n.text.RPG_SELF_GIVE};

                            case rpgConst.CODE_AMOUNT_NAN:
                                return {message:i18n.text.RPG_AMOUNT_NAN};

                            case rpgConst.CODE_AMOUNT_UNAVAILABLE:
                                return {message:i18n.text.RPG_AMOUNT_UNAVAILABLE};

                            case rpgConst.CODE_DB_ERROR:
                                return {message:i18n.text.DB_ERROR};

                            default:
                                return {message:i18n.text.DEFAULT_ERROR};
                        }
                    }
                }

            case 'revive':
                try {
                    let reviveStatus = await rpg.revive(member.id);
                    if (reviveStatus === true) {
                        return {message:i18n.text.RPG_SELF_REVIVE}
                    } else {
                        return {message:i18n.text.DEFAULT_ERROR};
                    }
                } catch (errorCode) {
                    switch (errorCode) {
                        case rpgConst.CODE_SELF_NOT_REGISTERED:
                            return {message:i18n.text.RPG_SELF_NOT_REGISTERED};

                        case rpgConst.CODE_NOT_DEAD:
                            return {message:i18n.text.RPG_USER_NOT_DEAD};

                        case rpgConst.CODE_NOT_ENOUGH_MONEY:
                            return {message:i18n.text.RPG_NOT_ENOUGH_MONEY};

                        case rpgConst.CODE_DB_ERROR:
                            return {message:i18n.text.DB_ERROR};

                        default:
                            return {message:i18n.text.DEFAULT_ERROR};
                    }
                }

            /** Default : Invalid command message */
            default:
                return {message:i18n.text.INVALID_COMMAND
                    .replace('{member}', member).replace('{command}', arguments[0])};
        }
    };

    let replyData =  await reply();
    if (replyData !== null) {
        if (replyData.noDelete !== true) {
            message.delete();
        }
        if (replyData.hasOwnProperty(channel) === true) {
            replyData.channel.send(replyData.message).catch(e => console.error(e));
        } else {
            channel.send(replyData.message).catch(e => console.error(e));
        }
    }
});

/**
 * Get a random integer between `min` and `max`.
 *
 * @param {number} min - min number
 * @param {number} max - max number
 * @return {int} a random integer
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/** Event handlers */
DiscordBotClient.on('ready', () => {
    if (!fs.existsSync(DB_DIR)){
        fs.mkdirSync(DB_DIR);
    }
    sqlite.open('./' + DB_DIR + DB_FILE +'.sqlite'
    ).then(
        () =>
            DiscordBotClient.user.setActivity(
            `#${config.channel} > ${PREFIX}help`,
            { type: 'WATCHING' }
            )
    ).then(
        () => console.info('I am ready!')
    ).catch(error => console.error(error));
});

DiscordBotClient.on('disconnect', (reason) => {
    console.info('Bot disconnected.');
    sqlite.close().then(() => {
        if (reason.code === 1000) {
            console.info('Bye bye.');
            process.exit();
        }
    });
});

DiscordBotClient.on('reconnecting', () => {console.info('Trying to reconnect.');});

DiscordBotClient.on('resume',() => {
    sqlite.open('./' + DB_DIR + DB_FILE +'.sqlite').
    then(() => console.info('Back online!')).catch(error => console.error(error));
});

/** Display errors in console */
DiscordBotClient.on('error',(e) => console.error('ERROR', e));
DiscordBotClient.on('warn', (e) => console.warn('WARN', e));
DiscordBotClient.on('debug',(e) => console.info('INFO', e));

/** Logs in the Discord bot */
DiscordBotClient.login(config.token).catch(error => console.error(error));