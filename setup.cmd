@echo off
setlocal ENABLEEXTENSIONS
set "bot_name=GLaDOS DISCORD BOT"
set config="config.json"
set cmd_color=12
title %bot_name%
color %cmd_color%
if exist %config% goto update
:install
echo Installing %bot_name%
start "Installing dependencies for %bot_name%" cmd /C npm install --save
echo Installing dependencies.
echo Please wait until the second command prompt closes before proceeding.
echo It seems like this is your first time using %bot_name%.
echo In order for the bot to function we need to know a couple of things.
echo Chose the prefix you want %bot_name% to respond to.
set /p prefix="">nul
echo Specify the channel the bot should be used in.
set /p channel="">nul
echo Enter your API Token ; If not yet created type in TOKEN and edit the %config% file.
set /p token="">nul
@echo {>%config%
@echo "prefix": "%prefix%",>>%config%
@echo "channel": "%channel%",>>%config%
@echo "token": "%token%">>%config%
@echo }>>%config%
echo Launch "start.cmd" to launch %bot_name%
echo Please ensure the API Token has been specified in the config.json, if not already done.
pause
exit
:update
echo Updating %bot_name%
start "Updating dependencies for %bot_name%" cmd /C npm install --save
echo Installing possible new dependencies.
start "Updating" cmd /C npm update
echo Updating dependencies.
echo After all other commands prompt are closed.
echo %bot_name% Will be usable.
pause
exit