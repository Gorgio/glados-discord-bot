# GLaDOS Bot
GLaDOS Bot is a Discord bot build with Discord.js which allows you and your server to have access
to a certain amount of commands.
## Requirements
GLaDOS Bot runs on Node.js 8.0+ please make sure you have the correct version installed.

https://nodejs.org/

## Installing & Updating GLaDOS Bot:
Just run the ``setup.cmd`` file.
If it's the first time it will download the bot and ask you some question to fill the ``config.json`` for you.

Otherwise it will update it's dependencies if needed.
## Running GLaDOS Bot:
Open ``run.cmd`` and you should be good to go.

## Usage:
 * help : Displays a help message.
 * ping : Answers with pong and provides the bot's heartbeat ping.
 * ask : Gives a random answer to a Yes/No question by fetching them in the ask.txt file 
 Answers are separated by a semi colon `;`.
 * age : Informs since how long the user has been on the server.
 * ageserv : Informs since how long does the server exists.
 * d{6,20,100} : Rolls a dice of {6,20,100} faces.
 * dstats : Provides user statistics for all the dices.
 
### RPG Game:
 * register : To register in the game.
 * profile [@mention] : To see your or the user mentioned profil.
 * list [page] : Lists all user participating in the RPG Game, up to 20 users per page.
 * daily : To receive your daily bonus.
 * attack @mention : Attacking the user mentioned.
 * heal [@mention] : Heal your self or mentioned user.
 * give @mention amount : Give the specified amount of money to the mentioned user.
 * revive : Revives your user for a cost if you died.

### Admin commands
Only usable by members of the guild who have the ``ADMINISTRATOR`` permission.
 * post : Mention a channel and the bot will post the message to the specific channel.
 If no channel mentioned it will send it to the default channel.
 * kill : Rapidly shutdowns the bot.
 