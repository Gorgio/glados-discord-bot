const moment = require('moment');
const rpg = require('./consts.js');
const sqlite = require('sqlite');

const CREATE_SQL_TABLE = 'CREATE TABLE IF NOT EXISTS player (userId TEXT PRIMARY KEY ASC, health_current INTEGER,' +
    'health_max INTEGER, energy_current INTEGER, energy_max INTEGER, attack INTEGER, magic INTEGER,' +
    'money INTEGER, level INTEGER, cost_next_level INTEGER, last_daily TEXT, start_time INTEGER)';

/**
 * Fetch user Data for given ID.
 */
function getUserData(userId) {
    return new Promise((resolve, reject) => {
        sqlite.get(
            'SELECT * FROM player WHERE userId = $userId',
            { $userId: String(userId) }
        ).then((row) => {
            if (row === undefined) {
                resolve(null);
            } else {
                resolve(row);
            }
        }).catch((e) => {
            reject(e);
        })
    });
}
/**
 * Register a player into the RPG Game.
 *
 * @param userId
 */
const register = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await getUserData(userId);
            if (data !== null) {
                resolve(false);
            } else {
                await Promise.all([
                    sqlite.run(
                        'INSERT INTO player (userId, health_current, health_max, energy_current, energy_max,' +
                        'attack, magic, money, level, cost_next_level, last_daily, start_time) VALUES ($userId,' +
                        '$health_current, $health_max, $energy_current, $energy_max, $attack, $magic, $money, $level,' +
                        '$level_cost, $last_daily, $date)',
                        {
                            $userId: String(userId),
                            $health_current: rpg.BASE_HEALTH,
                            $health_max: rpg.BASE_HEALTH,
                            $energy_current: rpg.BASE_ENERGY,
                            $energy_max: rpg.BASE_ENERGY,
                            $attack: rpg.BASE_ATTACK,
                            $magic: rpg.BASE_MAGIC,
                            $money: rpg.BASE_MONEY,
                            $level: rpg.BASE_LEVEL,
                            $level_cost: rpg.BASE_LEVEL_COST,
                            $last_daily: String('01/01/1990'),
                            $date: Number(Date.now())
                        }
                    )
                ]);

                resolve(true);
            }
        } catch (e) {
            console.info('No Database Table for `player`. Creating it...');
            sqlite.run(CREATE_SQL_TABLE).then(() => {
                console.info('Created `player` table.');
                reject(rpg.CODE_TABLE_NOT_EXITING);
            }).catch(e => {
                console.error(e);
                reject(rpg.CODE_DB_ERROR);
            });
        }
    });
};

/**
 * Fetches profile data of member in the DataBase.
 *
 * @param userId
 * @returns {Promise}
 */
let getProfile = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            resolve(await getUserData(userId));
        } catch (e) {
            console.info('No Database Table for `player`. Creating it...');
            sqlite.run(CREATE_SQL_TABLE).then(() => {
                console.info('Created `player` table.');
                reject(rpg.CODE_TABLE_NOT_EXITING);
            }).catch(error => {
                console.error(error);
                reject(rpg.CODE_DB_ERROR);
            });
        }
    });
};

/**
 * Return total number of users registered in the RPG Game.
 *
 * @returns {Promise}
 */
const getNbPlayers = () => {
  return new Promise((resolve, reject) => {
      sqlite.get('SELECT COUNT(userId) AS nbPlayers FROM player').then(row => {
          resolve(row['nbPlayers']);
      }).catch(error => {
          console.error(error);
          reject(rpg.CODE_DB_ERROR);
      });
  })
};

/**
 * Get the user list for the page provided if it exists.
 *
 * @param page
 * @param playersPerPage
 * @returns {Promise}
 */
const getList = (page, playersPerPage) => {
    return new Promise((resolve, reject) => {
        let offset = (page-1)* playersPerPage;
        sqlite.all(
            'SELECT userId FROM player ORDER BY start_time LIMIT $limit OFFSET $offset',
            { $limit:playersPerPage, $offset:offset }
        ).then(rows => {
           if (rows.length === 0) {
               resolve(null);
           } else {
               resolve(rows);
           }
       }).catch(error => {
           console.error(error);
           reject(rpg.CODE_DB_ERROR);
       });
    });
};

/**
 * Daily bonus for provided user.
 * Resets energy and gives money corresponding to rpg.DAILY_MONEY
 *
 * @param userId
 * @returns {Promise}
 */
const daily = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await getUserData(userId);
            if (data === null) {
                reject(rpg.CODE_SELF_NOT_REGISTERED);
            } else {
                let last_daily = moment(data['last_daily'], "DD-MM-YYYY");
                if (last_daily.isBefore(moment(), 'day') === false) {
                    reject(rpg.CODE_DAILY_TO_RECENT);
                } else {
                    let now = moment().format('DD/MM/YYYY');
                    await Promise.all([
                        sqlite.run(
                            'UPDATE player SET energy_current = energy_max, money = money+$dailyAdd, ' +
                            'last_daily = $now WHERE userId = $userId',
                            { $dailyAdd:rpg.DAILY_MONEY, $now:now, $userId:String(userId) }
                        )
                    ]);
                    resolve({amount:rpg.DAILY_MONEY});
                }
            }
        } catch (e) {
            console.info('No Database Table for `player`. Creating it...');
            sqlite.run(CREATE_SQL_TABLE).then(() => {
                console.info('Created `player` table.');
                reject(rpg.CODE_DB_ERROR);
            }).catch(error => console.error(error));
        }
    })
};

/**
 * Attacks `defender` user and wound him corresponding to `attacker`'s attack points.
 *
 * @param attackerId
 * @param defenderId
 * @returns {Promise}
 */
const attack = (attackerId, defenderId) => {
    return new Promise(async (resolve, reject) => {
        if (attackerId === defenderId) {
            reject(rpg.CODE_SELF_ATTACK);
        } else {
            try {
                let attackerData = await getUserData(attackerId);
                let defenderData = await getUserData(defenderId);
                if (attackerData === null) {
                    reject(rpg.CODE_SELF_NOT_REGISTERED);
                } else if (attackerData['health_current'] <= 0) {
                    reject(rpg.CODE_USER_IS_DEAD);
                } else if (attackerData['energy_current'] < rpg.ACTION_COST) {
                    reject(rpg.CODE_NOT_ENOUGH_ENERGY);
                } else if (defenderData === null) {
                    reject(rpg.CODE_MEMBER_NOT_REGISTERED);
                } else if (defenderData['health_current'] <= 0) {
                    reject(rpg.CODE_DEFENDER_ALREADY_DEAD);
                } else {
                    let newHealth = defenderData['health_current']-attackerData.attack;
                    let newEnergy = attackerData['energy_current']-rpg.ACTION_COST;
                    await Promise.all([
                        sqlite.run(
                            'UPDATE player SET health_current = $newHealth WHERE userId = $defenderId',
                            { $newHealth:newHealth, $defenderId:String(defenderId) }
                        ),
                        sqlite.run(
                            'UPDATE player SET energy_current = $newEnergy WHERE userId = $attackerId',
                            { $newEnergy:newEnergy, $attackerId:String(attackerId) }
                        )
                    ]);

                    resolve( {
                        damage:attackerData.attack,
                        health:newHealth,
                        healthMax:defenderData['health_max'],
                        energy:newEnergy
                    } );
                }
            } catch (e) {
                console.error(e);
                reject(rpg.CODE_DB_ERROR);
            }
        }
    });
};

/**
 * Heals `receiver` user corresponding to `caster`'s magic points.
 *
 * @param casterId
 * @param receiverId
 * @returns {Promise}
 */
const heal = (casterId, receiverId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let casterData = await getUserData(casterId);
            if (casterData === null) {
                reject(rpg.CODE_SELF_NOT_REGISTERED);
            } else if (casterData['health_current'] <= 0) {
                reject(rpg.CODE_USER_IS_DEAD);
            } else if (casterData['energy_current'] < rpg.ACTION_COST) {
                reject(rpg.CODE_NOT_ENOUGH_ENERGY);
            } else {
                let newEnergy = casterData['energy_current']-rpg.ACTION_COST;
                if (casterId === receiverId) {
                    let newHealth = casterData['health_current'] + casterData['magic'];
                    if (newHealth > casterData['health_max']) {
                        newHealth = casterData['health_max'];
                    }
                    if (casterData['health_current'] === casterData['health_max']) {
                        reject(rpg.CODE_CASTER_ALREADY_MAX_HEALTH);
                    } else {
                        await Promise.all([
                            sqlite.run(
                                'UPDATE player SET health_current = $newHealth, energy_current = $newEnergy ' +
                                'WHERE userId = $casterId',
                                { $newHealth:newHealth, $newEnergy:newEnergy, $casterId:String(casterId) }
                            )
                        ]);

                        resolve({
                            amountHealed:newHealth - casterData['health_current'],
                            health:newHealth,
                            healthMax:casterData['health_max'],
                            energy:newEnergy,self:true
                        });
                    }
                } else {
                    let receiverData = await getUserData(receiverId);
                    let newHealth = receiverData['health_current'] + casterData['magic'];
                    if (newHealth > receiverData['health_max']) {
                        newHealth = receiverData['health_max'];
                    }
                    if (receiverData === null) {
                        reject(rpg.CODE_MEMBER_NOT_REGISTERED);
                    } else if (receiverData['health_current'] === receiverData['health_max']) {
                        reject(rpg.CODE_RECEIVER_ALREADY_MAX_HEALTH);
                    } else if (receiverData['health_current'] <= 0) {
                        reject(rpg.CODE_RECEIVER_DEAD);
                    } else {
                        await Promise.all([
                            sqlite.run(
                                'UPDATE player SET health_current = $newHealth WHERE userId = $receiverId',
                                {$newHealth:newHealth,$receiverId:String(receiverId)}
                            ),
                            sqlite.run(
                                'UPDATE player SET energy_current = $newEnergy WHERE userId = $casterId',
                                {$newEnergy:newEnergy,$casterId:String(casterId)}
                            )
                        ]);

                        resolve({
                            amountHealed:newHealth - receiverData['health_current'],
                            health:newHealth,
                            healthMax:casterData['health_max'],
                            energy:newEnergy,self:false
                        });
                    }
                }
            }
        } catch (e) {
            console.error(e);
            reject(rpg.CODE_DB_ERROR);
        }
    });
};

/**
 * Gives an amount of money from giver's account to receiver's account.
 *
 * @param giverId
 * @param receiverId
 * @param amount
 * @returns {Promise}
 */
const give = (giverId, receiverId, amount) => {
    return new Promise(async (resolve, reject) => {
        try {
            let giverData = await getUserData(giverId);
            let receiverData = await getUserData(receiverId);
            if (giverId === receiverId) {
                reject(rpg.CODE_SELF_GIVE);
            } else if (giverData === null) {
                reject(rpg.CODE_SELF_NOT_REGISTERED);
            } else if (receiverData === null) {
                reject(rpg.CODE_MEMBER_NOT_REGISTERED);
            } else if (Number.isInteger(amount) === false) {
                reject(rpg.CODE_AMOUNT_NAN);
            } else if (giverData['money'] < amount) {
                reject(rpg.CODE_AMOUNT_UNAVAILABLE);
            } else {
                await Promise.all([
                    sqlite.run(
                        'UPDATE player SET money = money-$amount WHERE userId = $giverId',
                        { $amount:amount,$giverId:String(giverId) }
                    ),
                    sqlite.run(
                        'UPDATE player SET money = money+$amount WHERE userId = $receiverId',
                        { $amount:amount,$receiverId:String(receiverId) }
                    )
                ]);

                resolve(true);
            }
        } catch (e) {
            console.error(e);
            reject(rpg.CODE_DB_ERROR);
        }
    });
};

/**
 * Revives users for rpg.BASE_REVIVE_COST only when he is dead.
 *
 * @param userId
 * @returns {Promise}
 */
const revive = (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let data = await getUserData(userId);
            if (data === null) {
                reject(rpg.CODE_SELF_NOT_REGISTERED);
            } else if (data['health_current'] > 0) {
                reject(rpg.CODE_NOT_DEAD)
            } else if (data['money'] < rpg.BASE_REVIVE_COST) {
                reject(rpg.CODE_NOT_ENOUGH_MONEY);
            } else {
                await Promise.all([
                    sqlite.run(
                        'UPDATE player SET money = money-$cost, health_current = health_max WHERE userId = $userId',
                        { $cost:rpg.BASE_REVIVE_COST,$userId:userId}
                    )
                ]);

                resolve(true);
            }
        } catch (e) {
            console.error(e);
            reject(rpg.CODE_DB_ERROR);
        }
    })
};

module.exports.register     = register;
module.exports.getProfile   = getProfile;
module.exports.getNbPlayers = getNbPlayers;
module.exports.getList      = getList;
module.exports.daily        = daily;
module.exports.attack       = attack;
module.exports.heal         = heal;
module.exports.give         = give;
module.exports.revive       = revive;
