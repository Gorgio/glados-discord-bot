const sqlite = require('sqlite');

const CREATE_SQL_TABLE = 'CREATE TABLE IF NOT EXISTS rollStats (id INTEGER PRIMARY KEY ASC, userId TEXT,' +
    'rollType INTEGER, roll INTEGER)';
const CODE_DB_ERROR = 900;

/**
 * Inserts roll statistics into the database.
 *
 * @param userId
 * @param rollType
 * @param roll
 */
const insertRoll = function addRollStat(userId, rollType, roll)
{
    sqlite.run(`INSERT INTO rollStats (userId, rollType, roll) VALUES ($userId, $rollType, $roll)`,
        {$userId:userId, $rollType:rollType, $roll:roll}
    ).catch(() => {
        console.info('No Database Table for `rollStats`. Creating it...');
        sqlite.run(CREATE_SQL_TABLE).
        then(() => {
            console.info('Created `rollStats` table.');
            addRollStat(userId, rollType, roll);
        }).catch(error => console.error(error))
    });
};

/**
 * Fetches all dice roll statistics of user from the database.
 *
 * The promise return variable is an array or null
 *
 * @param userId
 * @returns {Promise}
 */
const getStats = function getAllRollStats(userId) {
    return new Promise((resolve, reject) => {
        sqlite.all('SELECT rollType, AVG(roll) AS "avg", COUNT(roll) AS "nbRoll" FROM rollStats ' +
            'WHERE userId = $userId GROUP BY rollType', {$userId:String(userId)}
        ).
        then(row => {
            if (row.length === 0) {
                resolve(null);
            } else {
                resolve(row);
            }
        }).catch(() =>  {
            console.info('No Database Table for `rollStats`. Creating it...');
            sqlite.run(CREATE_SQL_TABLE).
            then(() => {
                console.info('Created `rollStats` table.');
                return getStats(userId);
            }).catch(error => {
                console.error(error);
                reject(CODE_DB_ERROR);
            })
        });
    });
};

module.exports.getStats = getStats;
module.exports.insertRoll = insertRoll;
