const Discord = require('discord.js');
const Moment = require('moment');
const rpgConst = require('../glados-modules/rpg/consts.js');
/**
 * Text & Placeholder constants
 * @type {Object}
 */
const text = Object.freeze({
    DB_ERROR: 'Une erreur est survenue.',
    DEFAULT_ERROR: 'Une erreur est survenue.',
    AGE_SERVER: 'Le serveur existe depuis{range}. Il a était crée le {calendarDate}.',
    INVALID_COMMAND: `[{member}] "{command}" n'est pas une commande valide.`,

    RPG_REGISTER_SUCCESS:'[{member}] Vous êtes inscrit dans le RPG-Like.',
    RPG_ALREADY_REGISTERED:'[{member}] Vous êtes déjà inscrit.',
    RPG_SELF_NOT_REGISTERED:'Vous êtes pas inscrit au jeu.',
    RPG_MEMBER_NOT_REGISTERED: '{member} n\'est pas inscrit au jeu.',
    RPG_DAILY_TO_EARLY:'{member}, vous avez déjà reçu votre bonus quotidien.',
    RPG_DAILY_RECEIVED:'{member}, votre énergie est de nouveau pleine et vous avez reçu {amount} :tickets: !',
    RPG_USER_IS_DEAD:'Vous ne pouvez pas effectuer d\'action car vous êtes mort.',
    RPG_NOT_ENOUGH_ENERGY: 'Vous n\'avez pas assez d\'énergy pour effectuer cette action.',
    RPG_SPECIFY_USER_TO_ATTACK: 'Veuillez spécifier le joueur que vous voulez attaquer.',
    RPG_SELF_ATTACK: 'On se mutile pas ici.',
    RPG_MEMBER_ALREADY_DEAD: '{member} est déjà mort.',
    RPG_SELF_MAX_HEALTH: 'Vous n\'avez pas besoin d\'être soigné.',
    RPG_MEMBER_MAX_HEALTH: '{member} n\'a pas besoin d\'être soigné.',
    RPG_MEMBER_DEAD: 'Vous ne pouvez pas effectuer cette action sur {member} car il est mort.',
    RPG_SPECIFY_USER_TO_GIVE: 'Veuillez spécifier le joueur à qui vous voulez donner de l\'argent.',
    RPG_AMOUNT_TO_GIVE: 'Veuillez indiquer la somme que vous voulez donner.',
    RPG_SELF_GIVE: 'Vous ne pouvez pas vous donner vous même de l\'argent.',
    RPG_AMOUNT_NAN: 'Le montant que vous avez spécifié n\'est pas un nombre entier.',
    RPG_AMOUNT_UNAVAILABLE: 'Vous ne pouvez pas donner plus d\'argent que vous n\'avez.',
    RPG_USER_NOT_DEAD:'Vous n\'avez pas besoin d\'effectuer cette action en étant vivant.',
    RPG_NOT_ENOUGH_MONEY: 'Vous n\'avez pas assez de tickets pour effectuer cette action.',
    RPG_SELF_REVIVE: `Vous vous êtes ressuscité pour ${rpgConst.BASE_REVIVE_COST} :tickets:.`,
});

const AGE_USER = (member, joinedAt) => {
	let embed = new Discord.RichEmbed();
	embed.setTitle(getRelativeTimeFromNow(joinedAt,`Vous êtes arrivé le {calendarDate}.`))
        .setDescription(getRelativeTimeFromNow(joinedAt,`${member} vous êtes parmi nous depuis{range}.`)
    );
	return embed;
};

const DICE_ROLL_EMBED = (member, rollType, roll) => {
    return new Discord.RichEmbed()
        .addField(`:game_die: [**${rollType}**]`,`${member} a fait ${roll} !`);
};

const DICE_STATS_EMBED = (member, data) => {
    let embed = new Discord.RichEmbed()
        .setDescription(`:game_die: Statistique de ${member} au lancé de Dés :`);
    if (data === null) {
        embed.addField('Pas de statistique disponible.', 'Lancez un dé pour avoir des statistiques !')
    } else {
        for (let index = 0; index < data.length; ++index) {
            let avg = data[index]['avg'];
            let rollTpe = data[index]['rollType'];
            embed.addField(`**Dé ${rollTpe}**`,
                `Moyenne de ${String(formatNumber(avg))} sur ${data[index]['nbRoll']} lancés ` +
                `(${String(formatNumber((avg/rollTpe)*100))}%).`);
        }
    }
    return embed;
};

const HELP_EMBED = (prefix) => {
    return new Discord.RichEmbed()
        .setTitle(':small_blue_diamond: Fonctionnalités de GLaDOS :small_blue_diamond:')
        .setDescription(`Toutes les commandes doivent être préfixées de \`\`${prefix}\`\`.`)
        .addField(`${prefix}help`, 'Affiche ce message.')
        .addField(`${prefix}ping`, 'pong!')
        .addField(`${prefix}age`, 'Pour savoir depuis quand vous êtes sur le serveur.')
        .addField(`${prefix}ask`, 'Posez des questions fermées à GLaDOS. (*OUI/NON*)')
        .addField(`${prefix}d6 ; ${prefix}d20 ; ${prefix}d100`, 'Lancez un dé à 6, 20 ou 100 faces.')
        .addField(`${prefix}dstats`, 'Affiche vos statistiques au lancé de Dés.')
        .addField('Commandes RPG', `${prefix}helpgame`);
};

const HELP_RPG_EMBED = (prefix) => {
	return new Discord.RichEmbed()
		.setTitle('Commandes RPG')
		.addField(`${prefix}register`,'Pour vous inscrire au Jeu !')
		.addField(`${prefix}profile [rien/mention]`,':bust_in_silhouette: - Regardez vos stats ou celles d\'un autre joueur')
		.addField(`${prefix}attack [mention]`,':crossed_swords: - Attaquez un joueur !')
		.addField(`${prefix}heal [rien/mention]`,':green_heart: - Soignez vous ou soignez un joueur !')
		.addField(`${prefix}give [mention] [amount]`,':gift: - Donnez de l\'argent à un joueur.')
		.addField(
		    `${prefix}daily`,
            `:calendar_spiral: - Obtenez votre bonus quotidien : énergie rechargée :zap: et ${rpgConst.DAILY_MONEY} :tickets:`
        );
};

const PING_EMBED = (member, ping) => {
    return new Discord.RichEmbed()
        .setDescription(`[${member}] :ping_pong: Pong ! \`\`${String(Math.round(ping))}ms\`\``);
};

const RPG_PROFILE_EMBED = (member, data) => {
    return new Discord.RichEmbed()
        .setTitle(`Profile`)
        .setDescription(`${member}`)
        .addField(
            'Stats',
            `${data['health_current']}/${data['health_max']} :hearts:\n` +
                `ATK ${data['attack']} :crossed_swords:\nMagie ${data['magic']} :cyclone:\n` +
                `Energy ${data['energy_current']}/${data['energy_max']} :zap:\nMoney ${data['money']} :tickets:`
        );
};

const RPG_LIST_EMBED = (data, page, totalPages) => {
    let embed = new Discord.RichEmbed()
        .setTitle(`Liste des joueurs`);
    if (data === null) {
        if (page > 1) {
            embed.addField('Aucun joueur présent sur cette page.', 'Inviter d\'autre utilisateur à participer !')
        } else {
            embed.addField('Aucun joueur inscrit.', 'Soyer le premier à participer !')
        }
    } else {
        let text = '';
        for (let index = 0; index < data.length; ++index) {
            text += `- <@${data[index]['userId']}>\n`;
        }
        embed.setDescription(text);
        embed.setFooter(`Page ${page} / ${totalPages}`);
    }
    return embed;
};

const RPG_ATTACK_EMBED = (attacker, defender, attackData) => {
    return new Discord.RichEmbed()
        .setTitle(`:crossed_swords: Attaque`)
        .setDescription(
            `${attacker} a blessé ${defender} de ${attackData.damage} PV\n${defender} a maintenant ${attackData.health}/${attackData.healthMax} PV`
        )
        .setFooter(`Il vous reste ${attackData.energy} points d'énergie.`)
};

const RPG_ATTACK_KILL_EMBED = (defender, attackData) => {
    return new Discord.RichEmbed()
        .setTitle(':crossed_swords: Attaque :skull:')
        .setDescription(`Félicitation vous avez tué ${defender}!`)
        .setFooter(`Il vous reste ${attackData.energy} points d'énergie.`)
};

const RPG_HEAL_SELF_EMBED = (caster, healData) => {
    return new Discord.RichEmbed()
        .setTitle(`:green_heart: Auto-Soin`)
        .setDescription(
            `${caster} s'est soigné de ${healData.amountHealed} PV\n${caster} a maintenant ${healData.health}/${healData.healthMax} PV`
        )
        .setFooter(`Il vous reste ${healData.energy} points d'énergie.`)
};

const RPG_HEAL_MEMBER_EMBED = (caster, receiver, healData) => {
    return new Discord.RichEmbed()
        .setTitle(`:green_heart: Soin`)
        .setDescription(
            `${caster} a soigné ${receiver} de ${healData.amountHealed} PV\n${receiver} a maintenant ${healData.health}/${healData.healthMax} PV`
        )
        .setFooter(`Il vous reste ${healData.energy} points d'énergie.`)
};

const RPG_MONEY_EMBED = (giver, receiver, amount) => {
    return new Discord.RichEmbed()
        .setTitle(':gift: Donation')
        .setDescription(`${giver} a donné ${amount} :tickets: à ${receiver}.`)
};

const EMOJI_LIST_TEXT = 'Les emojis disponibles sont : ';
const EMOJI_UNKNOWN = 'L\'emoji que vous avez demandé n\'existe pas.' +
    ' Utilisez la commande ``list`` pour connaitre les emojis disponible.';

const STICKER_LIST_TEXT = 'Les stickers disponibles sont : ';
const STICKER_UNKNOWN = 'Le sticker que vous avez demandé n\'existe pas.' +
    ' Utilisez la commande ``list`` pour connaitre les stickers disponible.';

/**
 * Returns the placeholder where {range} get's changed into Y years MM months and DD days.
 * {calendarDate} is replaced by the date's calendar date DD/MM/Y
 *
 * @param {Date} date
 * @param {string} placeholder
 * @returns {string}
 */
function getRelativeTimeFromNow(date, placeholder) {
    let age = Moment.duration(Moment().diff(date));
    let years = age.years();
    let months = age.months();
    let days = age.days();
    let yearsText;
    let monthsText;
    let daysText;
    let text = '';
    let andText = 'et';
    if (years > 0) {
        yearsText =  ' ' + years + ' ';
        yearsText += years === 1 ? 'an' : 'ans';
    }
    if (months > 0) {
        monthsText =  ' ' + months + ' mois';
    }
    if (days > 0) {
        daysText = ' ' +days+' ';
        daysText += days === 1 ? 'jour' : 'jours';
    }
    if (yearsText !== undefined) {
        text += yearsText;
    }
    if (monthsText !== undefined) {
        if (daysText === undefined) {
            text += ' ' + andText;
        }
        text += monthsText;
    }
    if (daysText !== undefined) {
        if (monthsText !== undefined || yearsText !== undefined) {
            text += ' ' + andText;
        }
        text += daysText;
    }

    return placeholder.replace('{range}', text).replace('{calendarDate}', Moment(date).format('DD/MM/Y'));
}

/**
 * Formats number up to 2 decimal points if necessary.
 *
 * @param number
 * @returns {number}
 */
function formatNumber(number) {
    return (Math.round(number * 100))/100;
}

module.exports.text = text;
module.exports.AGE_USER = AGE_USER;
module.exports.DICE_ROLL_EMBED = DICE_ROLL_EMBED;
module.exports.DICE_STATS_EMBED = DICE_STATS_EMBED;
module.exports.HELP_EMBED = HELP_EMBED;
module.exports.HELP_RPG_EMBED = HELP_RPG_EMBED;
module.exports.PING_EMBED = PING_EMBED;
module.exports.RPG_PROFILE_EMBED = RPG_PROFILE_EMBED;
module.exports.RPG_LIST_EMBED = RPG_LIST_EMBED;
module.exports.RPG_ATTACK = RPG_ATTACK_EMBED;
module.exports.RPG_ATTACK_KILL = RPG_ATTACK_KILL_EMBED;
module.exports.RPG_HEAL_SELF = RPG_HEAL_SELF_EMBED;
module.exports.RPG_HEAL_MEMBER = RPG_HEAL_MEMBER_EMBED;
module.exports.RPG_MONEY = RPG_MONEY_EMBED;

module.exports.EMOJI_LIST = EMOJI_LIST_TEXT;
module.exports.EMOJI_UNKNOWN = EMOJI_UNKNOWN;

module.exports.STICKER_LIST = STICKER_LIST_TEXT;
module.exports.STICKER_UNKNOWN = STICKER_UNKNOWN;

module.exports.getRelativeTimeFromNow = getRelativeTimeFromNow;
